# Summary

The Plex Server is a well known server for media

## Container Information

This is typically called plex_container

| Variable name           | Description                                                       | Variable File stored |
|-------------------------|-------------------------------------------------------------------|----------------------|
| name                    |
| version                 |

## Infrastructure Information

This is typically called plex_infrastructure

| Variable name           | Description                                                       | Variable File stored |
|-------------------------|-------------------------------------------------------------------|----------------------|
| install_type            |
| site_name               |
| data_directory          |
| backup_directory        |
| config_directory        |
| transcode_directory     |
| directory_mount_type    | This can be set to NFS if you have files mounted on an NFS Server |

## Mounted Directories

There is a structure that is called plex_mounted_volumes where you can mount directories to the container.
The typical format that is used is:
    - {
        name: "media",
        source_server: "local",
        source_path: "/dev/disk/by-id/scsi-0QEMU_QEMU_HARDDISK_drive-scsi1-part1",
        target_path: "{{ plex_infrastructure.media_directory }}"
      }

This is typically called plex_nfs_volumes

Folder that can be mounted in NFS are:

| Folder name             | Description                                                                                |
|-------------------------|--------------------------------------------------------------------------------------------|
| media                   | Folders where your video is being played                                                   |
